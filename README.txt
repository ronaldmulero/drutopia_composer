
CONTENTS OF THIS FILE
---------------------

* Install Drutopia using Composer, Drupal Console, and Drush
* Setup Gitlab Dev Repos
* Module/Profile Code Update Kludge

INSTALL DRUTOPIA USING COMPOSER, DRUPAL CONSOLE, AND DRUSH
----------------------------------------------------------

1. Add your preferred IP address and host name combination to your /etc/hosts and apache vhosts.

2. Replace all occurrences of "your-drutopia-site.local" in the following command chain with your preferred host name:

composer create-project drupal/drupal your-drutopia-site.local ~8.2.3;cd your-drutopia-site.local;curl https://gitlab.com/ronaldmulero/drutopia_composer/raw/master/composer.json > composer.json;composer update;composer require drupal/console:~1.0 --prefer-dist --optimize-autoloader --sort-packages --no-update;composer update;drupal site:install;drush -y en features_ui drutopia_article drutopia_blog drutopia_comment drutopia_core drutopia_page drutopia_site drutopia_user

3. Run it!

4. Log in and have fun!

SETUP YOUR GITLAB DEV REPOS
---------------------------

1. Follow the instruction in the setup_drutopia_dev_repos.sh file included in this repo

MODULE/PROFILE CODE UPDATE KLUDGE
---------------------------------
The way that the composer.json file is setup is not ideal, but it's a start. Once we create a proper Drutopia Package, it will work much better.
In the meantime, to use composer to PULL Drutopia module/profile updates from the drutopia repos:

1. Delete the module/profile directory to be updated

2. Run the following command chain:

git reset composer.lock;git checkout composer.lock;composer clear-cache;composer update;composer update;../setup_drutopia_dev_repos.sh

# The above commands will revert the composer.lock changes, clear the composer cache, run composer update TWICE, and reset your gitlab dev repos.






