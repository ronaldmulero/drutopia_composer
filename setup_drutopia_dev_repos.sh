#!/bin/bash
# INSTRUCTIONS:
# 1. Replace occurrence of "your-drutopia-site.local" in this file.
# 2. Place this file one directory above "your-drutopia-site.local".
# 3. chmod 700 this file.
# 4. Run it!

cd your-drutopia-site.local/modules/contrib/drutopia_article
git remote rm composer
git remote set-url origin git@gitlab.com:drutopia/drutopia_article.git
cd ../drutopia_blog
git remote rm composer
git remote set-url origin git@gitlab.com:drutopia/drutopia_blog.git
cd ../drutopia_comment
git remote rm composer
git remote set-url origin git@gitlab.com:drutopia/drutopia_comment.git
cd ../drutopia_core
git remote rm composer
git remote set-url origin git@gitlab.com:drutopia/drutopia_core.git
cd ../drutopia_page
git remote rm composer
git remote set-url origin git@gitlab.com:drutopia/drutopia_page.git
cd ../drutopia_site
git remote rm composer
git remote set-url origin git@gitlab.com:drutopia/drutopia_site.git
cd ../drutopia_user
git remote rm composer
git remote set-url origin git@gitlab.com:drutopia/drutopia_user.git
cd ../../../profiles/drutopia
git remote rm composer
git remote set-url origin git@gitlab.com:drutopia/drutopia.git
